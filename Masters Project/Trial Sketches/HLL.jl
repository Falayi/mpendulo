### A Pluto.jl notebook ###
# v0.20.3

using Markdown
using InteractiveUtils

# ╔═╡ 8ec5d57e-ad7a-11ef-3d53-09c2d1ca970a
begin
using Probably
using Random
using Probably
using Random
using Base.Threads
end

# ╔═╡ bf89fc93-0034-497f-aed9-738cedc8127a
using BenchmarkTools

# ╔═╡ df1124b3-0a64-4818-a1d7-db5734e07d91
begin
using CSV
using DataFrames
end

# ╔═╡ 0377f7e9-48b9-43d8-8816-6bbd11a4f127
md"# Adding Libraries"

# ╔═╡ e70b8b96-c196-4f6a-85eb-859b1fe995db
md"# Adding data from CSV"

# ╔═╡ 4dec863a-76e1-41b8-a471-1b7ebfbad4ea
file_path = "C:/Users/Thabo/Desktop/Project/employee.csv" 

# ╔═╡ ea9d1323-2172-4971-bbf4-77fcf1e3702c
df = CSV.read(file_path, DataFrame)

# ╔═╡ 784d59ac-6963-4a9f-875c-ca7796e2bb86
elements2 = df[:, :"Full Name"]

# ╔═╡ 38ad6682-757d-4a6e-91f0-435478725c84
hll2 = HyperLogLog{12}() 

# ╔═╡ 4ff43f81-00f8-4a01-a27c-e96b3bb6e85e
for element2 in elements2
    push!(hll2, element2)
end

# ╔═╡ 72c9885b-fb36-4012-9ed9-3a63cf035d61
cardinality = length(hll2)

# ╔═╡ 95fc2732-8363-4e0e-8ffd-2add6001b4df
time_taken = @elapsed for element2 in elements2
    push!(hll2, element2)
end

# ╔═╡ 26207342-0195-4562-a297-d1bb634bbe4f
@benchmark cardinality

# ╔═╡ 5f50c414-b909-4d96-beff-d6654f93f6a3
md"# Parallelisation"

# ╔═╡ 943c77bf-a807-4b94-9384-e74584751964
begin
# Create the HyperLogLog estimator with precision of 12
hll3 = HyperLogLog{4}()

# Assuming df is a DataFrame and EEID is a column in it
elements3 = df[:, :"Full Name"]

# Function to push elements into HLL for parallel threads
function add_to_hll(elements3, hll3)
    for element3 in elements3
        push!(hll3, element3)
    end
end

# Split the list of elements among threads
chunk_size = ceil(Int, length(elements3) / nthreads())
element_chunks = [elements3[i:min(i + chunk_size - 1, length(elements3))] for i in 1:chunk_size:length(elements3)]

# Parallelize the computation: each thread processes a chunk of elements
@threads for i in 1:nthreads()
    add_to_hll(element_chunks[i], hll3)
end

# Final estimate of the cardinality
println(length(hll3))

end


# ╔═╡ 265d5bcf-3425-4148-b760-33ba863acb5b
time_taken1 = @elapsed for element3 in elements3
    push!(hll3, element3)
end

# ╔═╡ dbc92375-cce7-4e56-abdb-9295c421863e
throughput2 = length(elements3) / time_taken

# ╔═╡ 1fa4e6c8-a4d9-4f77-bbf8-0a85a1444feb
@benchmark length(hll3)

# ╔═╡ 8905dc05-acd9-494b-9338-15dfc2f0defb
X=[1000,1000]

# ╔═╡ 9afd674b-5928-4852-a07f-f669ddc9d72b
Y=[757,1145]

# ╔═╡ 407424ba-5b91-4fb9-ab86-3786f9bc249b
Y1=[907,1002]

# ╔═╡ d93a8719-faac-4072-9141-3ab8f05f0a61
Y2=[900,987]

# ╔═╡ 4f1c164b-5d3b-45a5-8fff-8cdf3360ddbc
begin
# Function to calculate AAE and ARE
function calculate_errors(X,Y)
    if length(X) != length(Y)
        throw(ArgumentError("Input arrays must have the same length"))
    end

    # Average Absolute Error
    aae = mean(abs.(X .- Y))

    # Average Relative Error
    # Handle potential division by zero
    are = mean(abs.(X .- Y) ./ abs.(X))

    return aae, are
end

aae, are = calculate_errors(X, Y)
println("Average Absolute Error: $aae")
println("Average Relative Error: $are")
end

# ╔═╡ 4bd7dc7a-c84b-4f14-a586-56ee88fea26c
begin
# Function to calculate AAE and ARE
function calculate_errors1(X,Y1)
    if length(X) != length(Y1)
        throw(ArgumentError("Input arrays must have the same length"))
    end

    # Average Absolute Error
    aae1 = mean(abs.(X .- Y1))

    # Average Relative Error
    # Handle potential division by zero
    are1 = mean(abs.(X .- Y1) ./ abs.(X))

    return aae1, are1
end

aae1, are1 = calculate_errors(X, Y1)
println("Average Absolute Error: $aae1")
println("Average Relative Error: $are1")
end

# ╔═╡ 7040a78c-2df7-4ee3-9ccc-f0f896bc6e84
begin
# Function to calculate AAE and ARE
function calculate_errors2(X,Y2)
    if length(X) != length(Y2)
        throw(ArgumentError("Input arrays must have the same length"))
    end

    # Average Absolute Error
    aae2 = mean(abs.(X .- Y2))

    # Average Relative Error
    # Handle potential division by zero
    are2 = mean(abs.(X .- Y2) ./ abs.(X))

    return aae2, are2
end

aae2, are2 = calculate_errors(X, Y2)
println("Average Absolute Error: $aae2")
println("Average Relative Error: $are2")
end

# ╔═╡ 34d263c9-d554-45f0-b4da-37fdb16bf6a7
# ╠═╡ disabled = true
#=╠═╡

throughput = length(elements2) / time_taken
  ╠═╡ =#

# ╔═╡ cffdbf97-be1e-4a33-8275-72c9712bc79f
throughput = length(elements2) / time_taken

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
CSV = "336ed68f-0bac-5ca0-87d4-7b16caf5d00b"
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
Probably = "2172800d-0309-5a57-a84f-d50c94757422"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[compat]
BenchmarkTools = "~1.5.0"
CSV = "~0.10.15"
DataFrames = "~1.7.0"
Probably = "~0.1.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.10.2"
manifest_format = "2.0"
project_hash = "1f7cf2237d9e732138d5d0735b69ecf6515fb7c5"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "f1dff6729bc61f4d49e140da1af55dcd1ac97b2f"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.5.0"

[[deps.CSV]]
deps = ["CodecZlib", "Dates", "FilePathsBase", "InlineStrings", "Mmap", "Parsers", "PooledArrays", "PrecompileTools", "SentinelArrays", "Tables", "Unicode", "WeakRefStrings", "WorkerUtilities"]
git-tree-sha1 = "deddd8725e5e1cc49ee205a1964256043720a6c3"
uuid = "336ed68f-0bac-5ca0-87d4-7b16caf5d00b"
version = "0.10.15"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "bce6804e5e6044c6daab27bb533d1295e4a2e759"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.6"

[[deps.Compat]]
deps = ["TOML", "UUIDs"]
git-tree-sha1 = "8ae8d32e09f0dcf42a36b90d4e17f5dd2e4c4215"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.16.0"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.1.0+0"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "abe83f3a2f1b857aac70ef8b269080af17764bbe"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.16.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "DataStructures", "Future", "InlineStrings", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrecompileTools", "PrettyTables", "Printf", "Random", "Reexport", "SentinelArrays", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "fb61b4812c49343d7ef0b533ba982c46021938a6"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.7.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "1d0a14036acb104d9e89698bd408f63ab58cdc82"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.20"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.FilePathsBase]]
deps = ["Compat", "Dates"]
git-tree-sha1 = "7878ff7172a8e6beedd1dea14bd27c3c6340d361"
uuid = "48062228-2e41-5def-b9a4-89aafe57970f"
version = "0.9.22"

    [deps.FilePathsBase.extensions]
    FilePathsBaseMmapExt = "Mmap"
    FilePathsBaseTestExt = "Test"

    [deps.FilePathsBase.weakdeps]
    Mmap = "a63ad114-7e13-5084-954f-fe012c677804"
    Test = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.InlineStrings]]
git-tree-sha1 = "45521d31238e87ee9f9732561bfee12d4eebd52d"
uuid = "842dd82b-1e85-43dc-bf29-5d0ee9dffc48"
version = "1.4.2"

    [deps.InlineStrings.extensions]
    ArrowTypesExt = "ArrowTypes"
    ParsersExt = "Parsers"

    [deps.InlineStrings.weakdeps]
    ArrowTypes = "31f734f8-188a-4ce0-8406-c8a06bd891cd"
    Parsers = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "0dc7b50b8d436461be01300fd8cd45aa0274b038"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.3.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "31e996f0a15c7b280ba9f76636b3ff9e2ae58c9a"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.4"

[[deps.LaTeXStrings]]
git-tree-sha1 = "dda21b8cbd6a6c40d9d02a73230f9d70fed6918c"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.4.0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "ec4f7fbeab05d7747bdf98eb74d130a2a2ed298d"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.2.0"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.23+4"

[[deps.OrderedCollections]]
git-tree-sha1 = "dfdf5519f235516220579f949664f1bf44e741c5"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.3"

[[deps.Parsers]]
deps = ["Dates", "PrecompileTools", "UUIDs"]
git-tree-sha1 = "8489905bcdbcfac64d1daa51ca07c0d8f0283821"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.8.1"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "36d8b4b899628fb92c2749eb488d884a926614d3"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.3"

[[deps.PrecompileTools]]
deps = ["Preferences"]
git-tree-sha1 = "5aa36f7049a63a1528fe8f7c3f2113413ffd4e1f"
uuid = "aea7be01-6a6a-4083-8856-8a6e6704d82a"
version = "1.2.1"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "9306f6085165d270f7e3db02af26a400d580f5c6"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.4.3"

[[deps.PrettyTables]]
deps = ["Crayons", "LaTeXStrings", "Markdown", "PrecompileTools", "Printf", "Reexport", "StringManipulation", "Tables"]
git-tree-sha1 = "1101cd475833706e4d0e7b122218257178f48f34"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "2.4.0"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Probably]]
git-tree-sha1 = "cbf5995b4afc36e2460a9bc4d8d84a3e246bcbbf"
uuid = "2172800d-0309-5a57-a84f-d50c94757422"
version = "0.1.2"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.Random]]
deps = ["SHA"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.SentinelArrays]]
deps = ["Dates", "Random"]
git-tree-sha1 = "d0553ce4031a081cc42387a9b9c8441b7d99f32d"
uuid = "91c51154-3ec4-41a3-a24f-3f23e20d615c"
version = "1.4.7"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "66e0a8e672a0bdfca2c3f5937efb8538b9ddc085"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.2.1"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"
version = "1.10.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.10.0"

[[deps.StringManipulation]]
deps = ["PrecompileTools"]
git-tree-sha1 = "a6b1675a536c5ad1a60e5a5153e1fee12eb146e3"
uuid = "892a3eda-7b42-436c-8928-eab12a02cf0e"
version = "0.4.0"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "7.2.1+1"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "OrderedCollections", "TableTraits"]
git-tree-sha1 = "598cd7c1f68d1e205689b1c2fe65a9f85846f297"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.12.0"

[[deps.TranscodingStreams]]
git-tree-sha1 = "0c45878dcfdcfa8480052b6ab162cdd138781742"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.11.3"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.WeakRefStrings]]
deps = ["DataAPI", "InlineStrings", "Parsers"]
git-tree-sha1 = "b1be2855ed9ed8eac54e5caff2afcdb442d52c23"
uuid = "ea10d353-3f73-51f8-a26c-33c1cb351aa5"
version = "1.4.2"

[[deps.WorkerUtilities]]
git-tree-sha1 = "cd1659ba0d57b71a464a29e64dbc67cfe83d54e7"
uuid = "76eceee3-57b5-4d4a-8e66-0e911cebbf60"
version = "1.6.1"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+1"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.8.0+1"
"""

# ╔═╡ Cell order:
# ╠═0377f7e9-48b9-43d8-8816-6bbd11a4f127
# ╠═8ec5d57e-ad7a-11ef-3d53-09c2d1ca970a
# ╠═bf89fc93-0034-497f-aed9-738cedc8127a
# ╠═e70b8b96-c196-4f6a-85eb-859b1fe995db
# ╠═4dec863a-76e1-41b8-a471-1b7ebfbad4ea
# ╠═df1124b3-0a64-4818-a1d7-db5734e07d91
# ╠═ea9d1323-2172-4971-bbf4-77fcf1e3702c
# ╠═784d59ac-6963-4a9f-875c-ca7796e2bb86
# ╠═38ad6682-757d-4a6e-91f0-435478725c84
# ╠═4ff43f81-00f8-4a01-a27c-e96b3bb6e85e
# ╠═72c9885b-fb36-4012-9ed9-3a63cf035d61
# ╠═95fc2732-8363-4e0e-8ffd-2add6001b4df
# ╠═cffdbf97-be1e-4a33-8275-72c9712bc79f
# ╠═34d263c9-d554-45f0-b4da-37fdb16bf6a7
# ╠═26207342-0195-4562-a297-d1bb634bbe4f
# ╠═5f50c414-b909-4d96-beff-d6654f93f6a3
# ╠═943c77bf-a807-4b94-9384-e74584751964
# ╠═265d5bcf-3425-4148-b760-33ba863acb5b
# ╠═dbc92375-cce7-4e56-abdb-9295c421863e
# ╠═1fa4e6c8-a4d9-4f77-bbf8-0a85a1444feb
# ╠═8905dc05-acd9-494b-9338-15dfc2f0defb
# ╠═9afd674b-5928-4852-a07f-f669ddc9d72b
# ╠═407424ba-5b91-4fb9-ab86-3786f9bc249b
# ╠═d93a8719-faac-4072-9141-3ab8f05f0a61
# ╠═4f1c164b-5d3b-45a5-8fff-8cdf3360ddbc
# ╠═4bd7dc7a-c84b-4f14-a586-56ee88fea26c
# ╠═7040a78c-2df7-4ee3-9ccc-f0f896bc6e84
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
